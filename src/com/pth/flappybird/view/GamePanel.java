package com.pth.flappybird.view;

import com.pth.flappybird.manager.GameManager;
import com.pth.flappybird.manager.ImageStore;
import com.pth.flappybird.model.HighScore;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class GamePanel extends BasePanel implements Runnable {
    private GameManager gameManager;
    private Thread thread;
    private JLabel jLabelMenu;
    private JLabel jLabelRestart;

    @Override
    public void initComponents() {
        setLayout(null);
        setBackground(Color.WHITE);
        setFocusable(true);
    }

    public void focus() {
        requestFocusInWindow();
    }

    @Override
    public void addComponents() {
        gameManager = new GameManager();
    }

    @Override
    public void registerListener() {
        KeyListener keyListener = new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                doKeyListener(e);
            }
        };
        addKeyListener(keyListener);
    }

    private void doKeyListener(KeyEvent keyEvent) {
        int keyCode = keyEvent.getKeyCode();
        switch (keyCode) {
            case KeyEvent.VK_SPACE:
                if (gameManager.getGameState() == GameManager.GAME_PLAY) {
                    gameManager.flyBird();
                }
                break;

            case KeyEvent.VK_S:
                if (gameManager.getGameState() == GameManager.GAME_START) {
                    gameManager.setGameState(GameManager.GAME_PLAY);
                    startGame();
                }
                break;

            default:
                break;
        }
    }

    public void startGame() {
        thread = new Thread(this);
        thread.start();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D graphics2D = (Graphics2D) g;
        graphics2D.drawImage(ImageStore.BIRD_BLUE, 0, 0, this);//draw bait
        gameManager.draw(graphics2D);
    }

    @Override
    public void run() {
        while (gameManager.getGameState() == GameManager.GAME_PLAY) {
            gameManager.move(System.currentTimeMillis());
            gameManager.checkGameOver();
            handleGameOver();
            gameManager.saveHighScore();
            if(gameManager.getPoint()==1000){
                thread.stop();
            }
            repaint();
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void handleGameOver() {
        if (gameManager.getGameState() == GameManager.GAME_OVER) {
            jLabelMenu = new JLabel();
            jLabelMenu.setBounds(270, 250, 114, 42);
            jLabelMenu.setIcon(ImageStore.IMG_MENU_NORMAL);
            add(jLabelMenu);
            jLabelMenu.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    onPanelStateListener.switchPanel("MenuPanel");
                    jLabelMenu.setIcon(ImageStore.IMG_MENU_NORMAL);
                }

                @Override
                public void mouseEntered(MouseEvent e) {
                    jLabelMenu.setIcon(ImageStore.IMG_MENU_ACTIVE);
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    jLabelMenu.setIcon(ImageStore.IMG_MENU_NORMAL);
                }
            });

            jLabelRestart = new JLabel();
            jLabelRestart.setBounds(400, 250, 114, 42);
            jLabelRestart.setIcon(ImageStore.IMG_RESTART_NORMAL);
            add(jLabelRestart);
            jLabelRestart.addMouseListener(new MouseAdapter() {

                @Override
                public void mouseEntered(MouseEvent e) {
                    jLabelRestart.setIcon(ImageStore.IMG_RESTART_ACTIVE);
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    jLabelRestart.setIcon(ImageStore.IMG_RESTART_NORMAL);
                }

                @Override
                public void mouseClicked(MouseEvent e) {
                    gameManager.setGameState(GameManager.GAME_START);
                    gameManager.initData();
                    jLabelRestart.setVisible(false);
                    jLabelMenu.setVisible(false);
                }
            });
        }
    }
}