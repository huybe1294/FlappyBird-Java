package com.pth.flappybird.view;

import com.pth.flappybird.manager.ImageStore;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MenuPanel extends BasePanel{
    private JLabel jLabelNewgame;
    private JLabel jLabelHighscore;
    private JLabel jLabelInstruction;
    private JLabel jLabelExit;

    @Override
    public void initComponents() {
        setLayout(null);
    }

    @Override
    public void addComponents() {
        jLabelNewgame=new JLabel();
        jLabelNewgame.setBounds(100,400,139,40);
        jLabelNewgame.setIcon(ImageStore.IMG_NEWGAME_NORMAL);
        add(jLabelNewgame);

        jLabelHighscore=new JLabel();
        jLabelHighscore.setBounds(250,400,139,40);
        jLabelHighscore.setIcon(ImageStore.IMG_HIGHSCORE_NORMAL);
        add(jLabelHighscore);

        jLabelInstruction=new JLabel();
        jLabelInstruction.setBounds(400,400,139,40);
        jLabelInstruction.setIcon(ImageStore.IMG_INSTRUCTION_NORMAL);
        add(jLabelInstruction);

        jLabelExit=new JLabel();
        jLabelExit.setBounds(550,400,139,40);
        jLabelExit.setIcon(ImageStore.IMG_EXIT_NORMAL);
        add(jLabelExit);
    }

    @Override
    public void registerListener() {
       jLabelNewgame.addMouseListener(new MouseAdapter() {

           @Override
           public void mouseEntered(MouseEvent e) {
               jLabelNewgame.setIcon(ImageStore.IMG_NEWGAME_ACTIVE);
           }

           @Override
           public void mouseExited(MouseEvent e) {
               jLabelNewgame.setIcon(ImageStore.IMG_NEWGAME_NORMAL);
           }

           @Override
           public void mouseClicked(MouseEvent e) {
               onPanelStateListener.switchPanel("GamePanel");
               jLabelNewgame.setIcon(ImageStore.IMG_NEWGAME_NORMAL);
           }
       });
       jLabelHighscore.addMouseListener(new MouseAdapter() {
           @Override
           public void mouseEntered(MouseEvent e) {
               jLabelHighscore.setIcon(ImageStore.IMG_HIGHSCORE_ACTIVE);
           }

           @Override
           public void mouseExited(MouseEvent e) {
               jLabelHighscore.setIcon(ImageStore.IMG_HIGHSCORE_NORMAL);
           }

           @Override
           public void mouseClicked(MouseEvent e) {
               onPanelStateListener.switchPanel("HighScorePanel");
               jLabelHighscore.setIcon(ImageStore.IMG_HIGHSCORE_NORMAL);
           }
       });
        jLabelInstruction.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                jLabelInstruction.setIcon(ImageStore.IMG_INSTRUCTION_ACTIVE);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                jLabelInstruction.setIcon(ImageStore.IMG_INSTRUCTION_NORMAL);
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                onPanelStateListener.switchPanel("InstructionPanel");
                jLabelInstruction.setIcon(ImageStore.IMG_INSTRUCTION_NORMAL);
            }
        });
        jLabelExit.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                jLabelExit.setIcon(ImageStore.IMG_EXIT_ACTIVE);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                jLabelExit.setIcon(ImageStore.IMG_EXIT_NORMAL);
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                System.exit(0);
            }
        });
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D graphics2D= (Graphics2D) g;
        graphics2D.drawImage(ImageStore.IMG_MENU_BACKGROUND,0,0,null);
    }

}


