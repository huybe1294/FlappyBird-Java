package com.pth.flappybird.view;

public interface Setup {
    void initComponents();

    void addComponents();

    void registerListener();
}
