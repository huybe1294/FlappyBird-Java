package com.pth.flappybird.view;



import javax.swing.*;

public abstract class BasePanel extends JPanel implements Setup {
    protected OnPanelStateListener onPanelStateListener;

    public BasePanel() {
        initComponents();
        addComponents();
        registerListener();
    }

    public void setOnPanelStateListener(OnPanelStateListener onPanelStateListener) {
        this.onPanelStateListener = onPanelStateListener;
    }
}
