package com.pth.flappybird.view;

import com.pth.flappybird.manager.GameManager;
import com.pth.flappybird.manager.ImageStore;
import com.pth.flappybird.model.HighScore;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by Lap trinh on 8/31/2017.
 */
public class HighScorePanel extends BasePanel {
    private JLabel jLabel;

    @Override
    public void initComponents() {
        setLayout(null);
        setBackground(Color.GRAY);
    }

    @Override
    public void addComponents() {
        jLabel = new JLabel();
        jLabel.setBounds(350, 420, 98, 42);
        jLabel.setIcon(ImageStore.IMG__BACK_NORMAL);
        add(jLabel);
    }

    @Override
    public void registerListener() {
        jLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                onPanelStateListener.switchPanel("MenuPanel");
                jLabel.setIcon(ImageStore.IMG__BACK_NORMAL);
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                jLabel.setIcon(ImageStore.IMG__BACK_ACTIVE);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                jLabel.setIcon(ImageStore.IMG__BACK_NORMAL);
            }
        });
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D graphics2D = (Graphics2D) g;
        drawScore(graphics2D);
    }

    private void drawScore(Graphics2D graphics2D) {
        graphics2D.setStroke(new BasicStroke(3));
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.setFont(new Font("Arial", Font.BOLD, 32));
        graphics2D.setColor(Color.WHITE);
        graphics2D.drawString("Điểm cao hiện tại", 10, 50);
        graphics2D.drawString("" + new HighScore().getHighScore(), 10, 100);
    }
}
