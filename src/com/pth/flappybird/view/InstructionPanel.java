package com.pth.flappybird.view;

import com.pth.flappybird.manager.ImageStore;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class InstructionPanel extends BasePanel {
    private JLabel jLabel;

    @Override
    public void initComponents() {
        setLayout(null);
    }

    @Override
    public void addComponents() {
        jLabel = new JLabel();
        jLabel.setBounds(350, 420, 98, 42);
        jLabel.setIcon(ImageStore.IMG__BACK_NORMAL);
        add(jLabel);
    }

    @Override
    public void registerListener() {
        jLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                onPanelStateListener.switchPanel("MenuPanel");
                jLabel.setIcon(ImageStore.IMG__BACK_NORMAL);
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                jLabel.setIcon(ImageStore.IMG__BACK_ACTIVE);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                jLabel.setIcon(ImageStore.IMG__BACK_NORMAL);
            }
        });
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D graphics2D = (Graphics2D) g;
        graphics2D.drawImage(ImageStore.IMG_INSTRUCTION_BACKGROUND, 0, 0, null);
    }
}
