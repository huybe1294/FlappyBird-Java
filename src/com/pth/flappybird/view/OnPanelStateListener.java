package com.pth.flappybird.view;

/**
 * Created by Lap trinh on 8/31/2017.
 */
public interface OnPanelStateListener {
    void switchPanel(String panelName);
}
