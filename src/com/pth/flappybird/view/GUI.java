package com.pth.flappybird.view;

import javax.swing.*;
import java.awt.*;

public class GUI extends JFrame implements Setup, OnPanelStateListener {
    public static final int WIDTH_FRAME = 800;
    public static final int HEIGHT_FRAME = 500;

    private Image icon;
    private MenuPanel menuPanel;
    private GamePanel gamePanel;
    private InstructionPanel instructionPanel;
    private HighScorePanel highScorePanel;

    public GUI() {
        initComponents();
        addComponents();
        registerListener();
    }

    @Override
    public void initComponents() {
        setTitle("Flappy Bird");
        setLayout(new CardLayout());
        setSize(WIDTH_FRAME, HEIGHT_FRAME);
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    @Override
    public void addComponents() {
        menuPanel = new MenuPanel();
        menuPanel.setOnPanelStateListener(this);
        add(menuPanel);

        gamePanel = new GamePanel();
        gamePanel.setOnPanelStateListener(this);

        instructionPanel = new InstructionPanel();
        instructionPanel.setOnPanelStateListener(this);

        highScorePanel = new HighScorePanel();
        highScorePanel.setOnPanelStateListener(this);

        String path = getClass().getResource("/res/drawable/bird_icon.png").getPath();
        icon = new ImageIcon(path).getImage();
        setIconImage(icon);
    }

    @Override
    public void registerListener() {
    }

    public void switchPanel(String panelName) {
        switch (panelName) {
            case "GamePanel":
                if (gamePanel == null) {
                    gamePanel = new GamePanel();
                }
                remove(menuPanel);
                add(gamePanel);
                revalidate();
                gamePanel.focus();
                break;

            case "HighScorePanel":
                if (highScorePanel == null) {
                    highScorePanel = new HighScorePanel();
                }
                remove(menuPanel);
                add(highScorePanel);
                revalidate();
                break;

            case "InstructionPanel":
                if (instructionPanel == null) {
                    instructionPanel = new InstructionPanel();
                }
                remove(menuPanel);
                add(instructionPanel);
                revalidate();
                break;

            case "MenuPanel":
                if (menuPanel == null) {
                    menuPanel = new MenuPanel();
                }
                if (gamePanel != null) {
                    remove(gamePanel);
                }
                if (instructionPanel != null) {
                    remove(instructionPanel);
                }
                if (highScorePanel != null) {
                    remove(highScorePanel);
                }
                add(menuPanel);
                revalidate();
                break;

            default:
                break;
        }
    }
}
