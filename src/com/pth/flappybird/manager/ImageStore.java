package com.pth.flappybird.manager;

import java.awt.*;
import javax.swing.*;

public class ImageStore {
    public static final Image BACKGROUND = getImage("background.png");
    public static final Image BIRD_BLUE = getImage("bird_blue.gif");
    public static final Image BIRD_ORANGE = getImage("bird_orange.gif");
    public static final Image BIRD_VIOLET = getImage("bird_violet.gif");
    public static final Image PIPE_BELOW = getImage("ongduoi.png");
    public static final Image PIPE_ABOVE = getImage("ongtren.png");
    public static final Image IMG_GROUND = getImage("ground.png");
    public static final Image IMG_COIN = getImage("coin.png");
    public static final Image IMG_MENU_BACKGROUND = getImage("img_menu_background.jpg");
    public static final Image IMG_INSTRUCTION_BACKGROUND = getImage("img_instruction_background.png");

    public static final ImageIcon IMG_NEWGAME_NORMAL=getImageIcon("img_newgame_normal.png");
    public static final ImageIcon IMG_NEWGAME_ACTIVE=getImageIcon("img_newgame_active.png");
    public static final ImageIcon IMG_HIGHSCORE_NORMAL=getImageIcon("img_highscore_normal.png");
    public static final ImageIcon IMG_HIGHSCORE_ACTIVE=getImageIcon("img_highscore_active.png");
    public static final ImageIcon IMG_INSTRUCTION_NORMAL=getImageIcon("img_instruction_normal.png");
    public static final ImageIcon IMG_INSTRUCTION_ACTIVE=getImageIcon("img_instruction_active.png");
    public static final ImageIcon IMG_EXIT_NORMAL=getImageIcon("img_exit_normal.png");
    public static final ImageIcon IMG_EXIT_ACTIVE=getImageIcon("img_exit_active.png");
    public static final ImageIcon IMG_MENU_NORMAL=getImageIcon("img_menu_normal.png");
    public static final ImageIcon IMG_MENU_ACTIVE=getImageIcon("img_menu_active.png");
    public static final ImageIcon IMG_RESTART_NORMAL=getImageIcon("img_restart_normal.png");
    public static final ImageIcon IMG_RESTART_ACTIVE=getImageIcon("img_restart_active.png");
    public static final ImageIcon IMG_HIGHSCORE_ICON=getImageIcon("img_highscore_icon.png");
    public static final ImageIcon IMG__BACK_NORMAL=getImageIcon("img_back_normal.png");
    public static final ImageIcon IMG__BACK_ACTIVE=getImageIcon("img_back_active.png");

    private static Image getImage(String name) {
        String path = ImageStore.class.getResource("/res/drawable/" + name).getPath();
        return new ImageIcon(path).getImage();
    }

    private static ImageIcon getImageIcon(String name){
        String path=ImageStore.class.getResource("/res/drawable/"+name).getPath();
        return new ImageIcon(path);
    }
}
