package com.pth.flappybird.manager;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.io.File;

public class SoundManager {
    public static final String FLAP_SOUND = SoundManager.class.getResource("/res/raw/flap.wav").getPath();
    public static final String HIT_SOUND = SoundManager.class.getResource("/res/raw/hit.wav").getPath();
    public static final String POINT_SOUND = SoundManager.class.getResource("/res/raw/point.wav").getPath();

    private AudioInputStream audioInputStream;
    private Clip clip;

    public void playSound(File path) {
        try {
            audioInputStream = AudioSystem.getAudioInputStream(path);
            clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            clip.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void flap() {
        playSound(new File(FLAP_SOUND));
    }

    public void hit() {
        playSound(new File(HIT_SOUND));
    }

    public void point() {
        playSound(new File(POINT_SOUND));
    }

}
