package com.pth.flappybird.manager;

import com.pth.flappybird.model.Bird;
import com.pth.flappybird.model.HighScore;
import com.pth.flappybird.model.Pipe;
import com.pth.flappybird.model.Ground;
import com.pth.flappybird.view.GUI;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameManager {
    public static final int GAME_START = 0;
    public static final int GAME_PLAY = 1;
    public static final int GAME_OVER = 2;

    private Bird bird;
    private List<Pipe> pipes;
    private List<Ground> grounds;
    private Random random;
    private int gameState = GAME_START;
    private int point = 0;
    private HighScore highScore;

    public GameManager() {
        pipes = new ArrayList<>();
        grounds = new ArrayList<>();
        random = new Random();
        highScore = new HighScore();
        initGround();
        initData();
    }

    public void initData() {
        initBirds();
        initPipes();
        point = 0;
    }

    private void initBirds() {
        int type = random.nextInt(3);
        if (bird == null) {
            bird = new Bird(100, 200, 40, 40, type);
        } else {
            bird.setX(100);
            bird.setY(200);
            bird.setType(type);
            bird.setSpeed(0.0);
        }
    }

    private void initPipes() {
        pipes.clear();
        int x = 500;
        int w = 50;
        int h = 540;
        int space = 100;
        for (int i = 0; i < 1000; i++) {
            int y = random.nextInt(101) + 200;
            Pipe pipe1 = new Pipe(x, y, w, h, 1);
            Pipe pipe2 = new Pipe(x, y - space - h, w, h, 0);
            x = x + 200;
            pipes.add(pipe1);
            pipes.add(pipe2);
        }
    }

    private void initGround() {
        int x = 0;
        for (int i = 0; i < 260; i++) {
            Ground rim = new Ground(x, 400, GUI.WIDTH_FRAME, GUI.HEIGHT_FRAME - 400);
            x = x + GUI.WIDTH_FRAME;
            grounds.add(rim);
        }
    }

    private void drawBird(Graphics2D graphics2D) {
        bird.draw(graphics2D);
    }

    private void drawPipe(Graphics2D graphics2D) {
        for (int i = 0; i < pipes.size(); i++) {
            pipes.get(i).draw(graphics2D);
        }
    }

    private void drawRim(Graphics2D graphics2D) {
        for (int i = 0; i < grounds.size(); i++) {
            grounds.get(i).draw(graphics2D);
        }
    }

    private void moveBird(long timeDelta) {
        bird.move(timeDelta);

    }

    private void movePipe() {
        for (int i = 0; i < pipes.size(); i++) {
            pipes.get(i).move();
            if (pipes.get(i).getX() + pipes.get(i).getW() < 0) {
                pipes.remove(i);
            }
        }
    }

    private void moveRim() {
        for (int i = 0; i < grounds.size(); i++) {
            grounds.get(i).move();
            if (grounds.get(i).getX() + grounds.get(i).getW() < 0) {
                grounds.remove(i);
            }
        }
    }

    public void flyBird() {
        bird.fly();
    }

    public void checkGameOver() {
        for (int i = 0; i < pipes.size(); i++) {
            Rectangle2D dest = new Rectangle();
            Rectangle2D.intersect(pipes.get(i).getRect(), bird.getRect(), dest);
            double width = dest.getWidth();
            double height = dest.getHeight();
            if (width > 0 && height > 5) {
                bird.soundManager.hit();
                gameState = GAME_OVER;
            }
        }
        if (bird.getY() + bird.getH() > 410) {
            bird.soundManager.hit();
            gameState = GAME_OVER;
        }
    }

    public void draw(Graphics2D graphics2D) {
        graphics2D.drawImage(ImageStore.BACKGROUND, 0, 0, null);
        drawBird(graphics2D);
        drawPipe(graphics2D);
        drawRim(graphics2D);

        Font font = new Font("Cooper Black", Font.BOLD, 36);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.setFont(font);
        graphics2D.setColor(Color.WHITE);
        String strStart = "Press S to start";
        int widthStrStart = graphics2D.getFontMetrics(font).stringWidth(strStart);
        int heightStrStart = graphics2D.getFontMetrics(font).getHeight();
        String strGameOver = "Game Over";
        int widthStrRestart = graphics2D.getFontMetrics(font).stringWidth(strGameOver);
        int heightStrRestart = graphics2D.getFontMetrics(font).getHeight();
        if (gameState == GAME_START) {
            graphics2D.drawString(strStart, (GUI.WIDTH_FRAME - widthStrStart) / 2, (GUI.HEIGHT_FRAME) / 2 - heightStrStart);
        } else if (gameState == GAME_OVER) {
            graphics2D.drawString(strGameOver, (GUI.WIDTH_FRAME - widthStrRestart) / 2, (GUI.HEIGHT_FRAME) / 2 - heightStrRestart);
        }
        graphics2D.drawString("Point: " + point, 0, 25);
        graphics2D.drawString("Best: " + highScore.getHighScore(), 0, 465);

        //handle Wingame
        String strEndgame = "You Are Winner";
        int widthStrEndgame = graphics2D.getFontMetrics(font).stringWidth(strEndgame);
        int heightStrEndgame = graphics2D.getFontMetrics(font).getHeight();
        if (point == 1000) {
            graphics2D.drawString(strEndgame, (GUI.WIDTH_FRAME - widthStrEndgame) / 2, (GUI.HEIGHT_FRAME) / 2 - heightStrEndgame);
        }
    }

    public void move(long deltaTime) {
        moveBird(deltaTime);
        movePipe();
        moveRim();
        for (int i = 0; i < pipes.size(); i++) {
            if (bird.getX() > pipes.get(i).getX() + 25 && !pipes.get(i).getBehindPipe() && i % 2 == 1) {
                point++;
                bird.soundManager.point();
                pipes.get(i).setBehindPipe(true);
            }
        }
    }

    public void saveHighScore() {
        if (gameState == GAME_OVER) {
            int pointSave = point;
            if (pointSave > highScore.getHighScore()) {
                highScore.setNewHighScore(pointSave);
            }
        }
    }

    public int getGameState() {
        return gameState;
    }

    public void setGameState(int gameState) {
        this.gameState = gameState;
    }

    public int getPoint() {
        return point;
    }
}
