package com.pth.flappybird.model;

import java.awt.*;

public abstract class GameObject {
    protected int x, y, w, h;

    public GameObject(int x, int y, int w, int h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    public abstract void draw(Graphics2D graphics2D);

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH(){
        return h;
    }

    public int getW() {
        return w;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
}
