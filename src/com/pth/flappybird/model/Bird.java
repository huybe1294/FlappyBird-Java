package com.pth.flappybird.model;

import com.pth.flappybird.manager.ImageStore;
import com.pth.flappybird.manager.SoundManager;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.io.File;

public class Bird extends GameObject {
    public static final int BIRD_BLUE = 0;
    public static final int BIRD_ORANGE = 1;
    public static final int BIRD_VIOLET = 2;

    private int type;
    private double gravity = 0.1;
    private double speed = 0;
    public SoundManager soundManager;

    public Bird(int x, int y, int w, int h, int type) {
        super(x, y, w, h);
        this.type = type;
        soundManager=new SoundManager();
    }


    @Override
    public void draw(Graphics2D graphics2D) {
        switch (type) {
            case BIRD_BLUE:
                graphics2D.drawImage(ImageStore.BIRD_BLUE, x, y, w, h, null);
                break;
            case BIRD_ORANGE:
                graphics2D.drawImage(ImageStore.BIRD_ORANGE, x, y, w, h, null);
                break;
            case BIRD_VIOLET:
                graphics2D.drawImage(ImageStore.BIRD_VIOLET, x, y, w, h, null);
                break;
            default:
                break;
        }

    }

    public void move(long timeDelta) {
        speed += gravity;
        int newY= (int) (this.y+speed);
        if(newY<0){
            return;
        }
        this.setY((int) (this.getY() + speed));
    }

    public void fly() {
        speed = -2.0;
        soundManager.flap();
    }

    public Rectangle2D getRect() {
        return new Rectangle(x, y, w, h);
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public void setType(int type) {
        this.type = type;
    }
}
