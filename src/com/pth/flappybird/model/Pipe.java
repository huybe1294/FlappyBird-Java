package com.pth.flappybird.model;

import com.pth.flappybird.manager.ImageStore;

import java.awt.*;
import java.awt.geom.Rectangle2D;

public class Pipe extends GameObject {
    public static final int PIPE_ABOVE = 0;
    public static final int PIPE_BELOW = 1;

    private int type;
    private boolean behindPipe = false;

    public Pipe(int x, int y, int w, int h, int type) {
        super(x, y, w, h);
        this.type = type;
    }

    @Override
    public void draw(Graphics2D graphics2D) {
        switch (type) {
            case PIPE_ABOVE:
                graphics2D.drawImage(ImageStore.PIPE_ABOVE, x, y, w, h, null);
                break;
            case PIPE_BELOW:
                graphics2D.drawImage(ImageStore.PIPE_BELOW, x, y, w, h, null);
                break;
            default:
                break;
        }
    }

    public void move() {
        x--;
    }

    public Rectangle2D getRect() {
        return new Rectangle(x, y, w, h);
    }

    public boolean getBehindPipe() {
        return behindPipe;
    }

    public void setBehindPipe(boolean behindPipe) {
        this.behindPipe = behindPipe;
    }
}


