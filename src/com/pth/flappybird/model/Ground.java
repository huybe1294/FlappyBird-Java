package com.pth.flappybird.model;

import com.pth.flappybird.manager.ImageStore;

import java.awt.*;

public class Ground extends GameObject {
    public Ground(int x, int y, int w, int h) {
        super(x, y, w, h);
    }

    @Override
    public void draw(Graphics2D graphics2D) {
        graphics2D.drawImage(ImageStore.IMG_GROUND,x,y,w,h,null);
    }

    public void move(){
        x--;
    }
}
