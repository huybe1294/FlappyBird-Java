package com.pth.flappybird.model;

import java.io.*;

public class HighScore {
    private static final String PATHSRC = HighScore.class.getResource("/res/assets/highscore.dat").getPath();

    private File file;
    private int highScore;

    public HighScore() {
        file = new File(PATHSRC);
        readFile();
    }

    public int getHighScore() {
        return highScore;
    }

    public void readFile() {
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            byte buff[] = new byte[1024];
            int length = fileInputStream.read(buff);
            while (length > 0) {
                String text = new String(buff, 0, length);
                highScore = Integer.parseInt(text);
                length = fileInputStream.read();
            }
            fileInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setNewHighScore(int newHighScore) {
        highScore = newHighScore;
        String src = newHighScore + "";
        try {
            FileOutputStream fOut = new FileOutputStream(file);
            byte buff[] = src.getBytes();
            fOut.write(buff);
            fOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
